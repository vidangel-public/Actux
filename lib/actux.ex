require Logger

defmodule Actux do
  @moduledoc """
    This module contains a simple function that you can use to transmit logs or
    analytics for your Elixir applications to actus.

    ### Configuring

    You can set your host and namespace by configuring the :actux application.

    ```elixir
    config :actux,
      actus_host: System.get_env("ACTUS_HOST"),
      actus_namespace: :my_namespace
    ```

    If you do not configure the application, then the defaults will be:
    - actus_host: `"0.0.0.0"`
    - actus_namespace: `:event`
  """
  @default_host "0.0.0.0"
  @default_namespace :event

  def client() do
    middleware = [
      {Tesla.Middleware.BaseUrl, Application.get_env(:actux, :actus_host, @default_host)},
      Tesla.Middleware.JSON,
      {Tesla.Middleware.Timeout, timeout: 5_000}
    ]

    Tesla.client(middleware)
  end

  def path(nil, table), do: path(Application.get_env(:actux, :actus_namespace, @default_namespace), table)
  def path(namespace, table) do
    "/#{namespace}/#{table}"
  end

  def push(namespace \\ nil, table, data) do
    spawn(fn ->
      case Tesla.post(client(), path(namespace, table), data) do
        {:error, reason} -> Logger.error("Error posting to /#{namespace}/#{table}: #{reason}")
        _ -> nil
      end
    end)

    {:ok, 201}
  end
end
